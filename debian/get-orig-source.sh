#!/bin/sh

set -e

WORKDIR=$(pwd)
TEMPDIR=$(mktemp --directory)
cd "$TEMPDIR"
hg clone https://hg.prosody.im/prosody-modules/
cd prosody-modules
VERSION=0.0~hg$(hg log --limit 1 \
		   --template "{date(date, '%Y%m%d')}.{node|short}")+dfsg
cd ..
mv -i prosody-modules prosody-modules-"$VERSION"
tar --bzip2 --create --exclude='.hg' --exclude='*.min.css' --exclude='*.min.js' \
    --file prosody-modules_"$VERSION".orig.tar.bz2 prosody-modules-"$VERSION"/
mv -i prosody-modules_"$VERSION".orig.tar.bz2 "$WORKDIR"/../
rm -rf "$TEMPDIR"
