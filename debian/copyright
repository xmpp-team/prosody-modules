Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: prosody-modules
Upstream-Contact: Matthew James Wild <me@matthewwild.co.uk>
Source: https://hg.prosody.im/prosody-modules
Files-Excluded: */*.min.js */*.min.css

Files: *
Copyright: 2009-2018 Various Contributors
License: Expat

Files: debian/*
Copyright: 2015 Enrico Tassi <gareuselesinge@debian.org>
           2015-2018 Victor Seva <vseva@debian.org>
License: Expat

Files: debian/missing-sources/platform.js
Copyright: 2014-2020 Benjamin Tan
           2011-2013 John-David Dalton
License: Expat

Files: debian/missing-sources/qrcode.js
Copyright: 2012-2015 davidshimjs
License: Expat

Files: mod_auth_token/*
Copyright: 2018 Minddistrict
License: Expat

Files: mod_s2s_auth_fingerprint/*
Copyright: 2013-2014 Kim Alvefur
License: Expat

Files: mod_delegation/*
Copyright: 2015 Jérôme Poisson
License: Expat

Files: mod_captcha_registration/*
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
License: Expat

Files: mod_captcha_registration/modules/mod_register.lua
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
           2014 mrDoctorWho
License: Expat

Files: mod_motd_sequential/*
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
           2010 Jeff Mitchell
License: Expat

Files: mod_group_bookmarks/*
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
License: Expat

Files: mod_component_roundrobin/*
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
License: Expat

Files: mod_register_json/*
Copyright: 2010 - 2013, Marco Cirillo (LW.Org)
License: Expat

Files: mod_storage_xmlarchive/*
Copyright: 2015 Kim Alvefur
License: Expat

Files: mod_auth_any/*
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
License: Expat

Files: mod_csi_battery_saver/*
Copyright: 2016 Kim Alvefur
           2017 Thilo Molitor
License: Expat

Files: mod_default_bookmarks/*
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
           2011 Kim Alvefur
License: Expat

Files: mod_storage_lmdb/*
Copyright: 2015 Kim Alvefur
License: Expat

Files: mod_candy/*
Copyright: 2013 Kim Alvefur
License: Expat

Files: mod_http_dir_listing/*
Copyright: 2012 Kim Alvefur
License: Expat

Files: mod_auth_sql/*
Copyright: 2011 Tomasz Sterna <tomek@xiaoka.com>
           2011 Waqas Hussain <waqas20@gmail.com>
License: Expat

Files: mod_muc_log/*
Copyright: 2009 Thilo Cestonaro
           2009 Waqa Hussain
           2009-2013 Matthew Wild
           2013 Kim Alvefur
           2013 Marco Cirillo
License: Expat

Files: mod_adhoc_blacklist/*
Copyright: 2015 Kim Alvefur
License: Expat

Files: mod_admin_probe/*
Copyright: 2014 Florian Zeitz
License: Expat

Files: mod_pep_vcard_avatar/*
Copyright: 2008-2014 Matthew Wild
           2008-2014 Waqas Hussain
           2014 Kim Alvefur
License: Expat

Files: mod_admin_web/*
Copyright: 2010 Florian Zeitz
License: Expat

Files: mod_client_certs/*
Copyright: 2012 Thijs Alkemade
License: Expat

Files: mod_turncredentials/*
Copyright: 2012-2013 Philipp Hancke
License: Expat

Files: mod_admin_message/*
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
           2012-2013 Mikael Berthe
License: Expat

Files: mod_auth_joomla/*
Copyright: 2011 Waqas Hussain
License: Expat

Files: mod_storage_ldap/*
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
           2012 Rob Hoelz
License: Expat

Files: mod_omemo_all_access/*
Copyright: 2017 Daniel Gultsch
License: Expat

Files: mod_private_adhoc/*
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
License: Expat

Files: mod_admin_blocklist/*
Copyright: 2015 Kim Alvefur
License: Expat

Files: mod_proctitle/*
Copyright: 2015 Rob Hoelz
License: Expat

Files: mod_idlecompat/*
Copyright: 2014 Tobias Markmann
License: Expat

Files: mod_s2s_auth_dane/*
Copyright: 2013-2014 Kim Alvefur
License: Expat

Files: mod_auth_ccert/*
Copyright: 2013 Kim Alvefur
License: Expat

Files: mod_auth_dovecot/*
Copyright: 2010-2011 Waqas Hussain
           2011 Kim Alvefur
License: Expat

Files: mod_auth_dovecot/auth_dovecot/sasl_dovecot.lib.lua
Copyright: 2008-2009 Tobias Markmann
           2010 Javie Torres
           2010-2011 Matthew Wild
           2010-2011 Waqas Hussain
           2011 Kim Alvefur
License: BSD-3-clause

Files: mod_auth_external/*
Copyright: 2010 Waqas Hussain
           2010 Jef Mitchell
           2013 Mikael Nordfeldth
           2013 Matthew Wild, finally came to fix it all
License: Expat

Files: mod_mam_muc/*
Copyright: 2011-2014 Kim Alvefur
License: Expat

Files: mod_email_pass/*
Copyright: 2011-2012 Kim Alvefur
License: Expat

Files: mod_http_upload/*
Copyright: 2015 Kim Alvefur
License: Expat

Files: mod_http_upload_external/*
Copyright: 2015-2016 Kim Alvefur
License: Expat

Files: mod_smacks/*
Copyright: 2010-2015 Matthew Wild
           2010 Waqa Hussain
           2012-2015 Kim Alvefur
           2012 Thijs Alkemade
           2014 Florian Zeitz
License: Expat

Files: mod_statsd/*
Copyright: 2014 Daurnimator
License: Expat

Files: mod_auth_phpbb3/*
Copyright: 2011 Waqas Hussain
License: Expat

Files: mod_auth_ldap2/*
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
           2012 Rob Hoelz
License: Expat

Files: mod_lib_ldap/*
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
           2012 Rob Hoelz
License: Expat

Files: mod_service_directories/*
Copyright: 2011 Waqas Hussain
License: Expat

Files: mod_conformance_restricted/*
Copyright: 2012 Florian Zeitz
License: Expat

Files: mod_onhold/*
Copyright: 2008-2009 Matthew Wild
           2008-2009 Waqas Hussain
           2009 Jeff Mitchell
License: Expat

Files: mod_list_inactive/*
Copyright: 2012-2013 Kim Alvefur
License: Expat

Files: mod_auth_http_async/*
Copyright: 2008-2013 Matthew Wild
           2008-2013 Waqas Hussain
           2014 Kim Alvefur
License: Expat

Files: mod_bidi/*
Copyright: 2013 Kim Alvefur
License: Expat

Files: mod_password_policy/*
Copyright: 2012 Waqas Hussain
License: Expat

Files: mod_auth_imap/*
Copyright: 2011 Kim Alvefur
License: Expat

Files: mod_carbons/*
Copyright: 2011 Kim Alvefur
License: Expat

Files: mod_vjud/*
Copyright: 2011-2012 Kim Alvefur
License: Expat

Files: mod_pubsub_hub/*
Copyright: 2011 - 2012 Kim Alvefur
License: Expat

Files: mod_carbons_copies/*
Copyright: 2012 Michael Holzt
License: Expat

Files: mod_auth_internal_yubikey/*
Copyright: 2008-2010 Matthew Wild
           2008-2010 Waqas Hussain
License: Expat

Files: mod_storage_gdbm/*
Copyright: 2014-2015 Kim Alvefur
License: Expat

Files: mod_auth_ha1/*
Copyright: 2014 Matthew Wild
License: Expat

Files: mod_sms_clickatell/*
Copyright: The Guy Who Wrote mod_twitter
           2011 Phil Stewart
License: Expat

Files: mod_log_messages_sql/*
Copyright: 2011-2012 Kim Alvefur
License: Expat

Files: mod_carbons_adhoc/*
Copyright: 2012 Michael Holzt
License: Expat

Files: mod_auth_pam/*
Copyright: 2013 Kim Alvefur
License: Expat

Files: mod_privacy_lists/*
Copyright: 2009-2014 Matthew Wild
           2009-2014 Waqas Hussain
           2009 Thilo Cestonaro
License: Expat

Files: mod_mam/*
Copyright: 2011-2014 Kim Alvefur
License: Expat

Files: mod_privilege/*
Copyright: 2015 Jérôme Poisson
License: Expat

Files: mod_auth_wordpress/*
Copyright: 2011 Waqas Hussain
           2011 Kim Alvefur
License: Expat

Files: mod_mamsub/*
Copyright: 2015 Kim Alvefur
License: Expat

Files: mod_auth_custom_http/*
Copyright: 2008-2010 Waqas Hussain
License: Expat

Files: mod_benchmark_storage/*
Copyright: 2015 Kim Alvefur
License: Expat

Files: mod_roster_command/*
Copyright: 2011 Matthew Wild
           2011 Adam Nielsen
License: Expat

Files: mod_swedishchef/*
Copyright: 2009 Florian Zeitz
           2009 Matthew Wild
License: Expat

Files: mod_captcha_registration/FiraSans-Regular.ttf
Copyright: 2012-2013, The Mozilla Foundation and Telefonica S.A.
License: Open-Font-1.1
  -----------------------------------------------------------
  SIL OPEN FONT LICENSE Version 1.1 - 26 February 2007
  -----------------------------------------------------------
  .
  PREAMBLE
  The goals of the Open Font License (OFL) are to stimulate worldwide
  development of collaborative font projects, to support the font creation
  efforts of academic and linguistic communities, and to provide a free and
  open framework in which fonts may be shared and improved in partnership
  with others.
  .
  The OFL allows the licensed fonts to be used, studied, modified and
  redistributed freely as long as they are not sold by themselves. The
  fonts, including any derivative works, can be bundled, embedded,
  redistributed and/or sold with any software provided that any reserved
  names are not used by derivative works. The fonts and derivatives,
  however, cannot be released under any other type of license. The
  requirement for fonts to remain under this license does not apply
  to any document created using the fonts or their derivatives.
  .
  DEFINITIONS
  "Font Software" refers to the set of files released by the Copyright
  Holder(s) under this license and clearly marked as such. This may
  include source files, build scripts and documentation.
  .
  "Reserved Font Name" refers to any names specified as such after the
  copyright statement(s).
  .
  "Original Version" refers to the collection of Font Software components as
  distributed by the Copyright Holder(s).
  .
  "Modified Version" refers to any derivative made by adding to, deleting,
  or substituting -- in part or in whole -- any of the components of the
  Original Version, by changing formats or by porting the Font Software to a
  new environment.
  .
  "Author" refers to any designer, engineer, programmer, technical
  writer or other person who contributed to the Font Software.
  .
  PERMISSION & CONDITIONS
  Permission is hereby granted, free of charge, to any person obtaining
  a copy of the Font Software, to use, study, copy, merge, embed, modify,
  redistribute, and sell modified and unmodified copies of the Font
  Software, subject to the following conditions:
  .
  1) Neither the Font Software nor any of its individual components,
  in Original or Modified Versions, may be sold by itself.
  .
  2) Original or Modified Versions of the Font Software may be bundled,
  redistributed and/or sold with any software, provided that each copy
  contains the above copyright notice and this license. These can be
  included either as stand-alone text files, human-readable headers or
  in the appropriate machine-readable metadata fields within text or
  binary files as long as those fields can be easily viewed by the user.
  .
  3) No Modified Version of the Font Software may use the Reserved Font
  Name(s) unless explicit written permission is granted by the corresponding
  Copyright Holder. This restriction only applies to the primary font name as
  presented to the users.
  .
  4) The name(s) of the Copyright Holder(s) or the Author(s) of the Font
  Software shall not be used to promote, endorse or advertise any
  Modified Version, except to acknowledge the contribution(s) of the
  Copyright Holder(s) and the Author(s) or with their explicit written
  permission.
  .
  5) The Font Software, modified or unmodified, in part or in whole,
  must be distributed entirely under this license, and must not be
  distributed under any other license. The requirement for fonts to
  remain under this license does not apply to any document created
  using the Font Software.
  .
  TERMINATION
  .
  This license becomes null and void if any of the above conditions are
  not met.
  .
  DISCLAIMER
  THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
  OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL THE
  COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
  INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
  DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM
  OTHER DEALINGS IN THE FONT SOFTWARE.

Files: mod_invites_page/static/illus-empty.svg
Copyright: 2020 Katerina Limpitsouni
License:
  All images, assets and vectors published on unDraw can be used for free.
  You can use them for noncommercial and commercial purposes. You do not
  need to ask permission from or provide credit to the creator or unDraw.
  .
  More precisely, unDraw grants you an nonexclusive, worldwide copyright
  license to download, copy, modify, distribute, perform, and use the
  assets provided from unDraw for free, including for commercial purposes,
  without permission from or attributing the creator or unDraw. This
  license does not include the right to compile assets, vectors or images
  from unDraw to replicate a similar or competing service, in any form or
  distribute the assets in packs or otherwise. This extends to automated
  and non-automated ways to link, embed, scrape, search or download the
  assets included on the website without our consent.

License: Expat
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions
  .
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.

License: BSD-3-clause
  Copyright (c) <year> <owner> . All rights reserved.
  .
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  .
  1. Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
  .
  2. Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
  .
  3. Neither the name of the copyright holder nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
